# DF Bench [![pipeline status](https://gitlab.com/bluehood/df_bench/badges/master/pipeline.svg)](https://gitlab.com/bluehood/df_bench/pipelines)
[RDataFrame](https://root.cern.ch/doc/master/classROOT_1_1RDataFrame.html) "micro"-benchmarks that have not made it to [rootbench](https://github.com/root-project/rootbench) yet.

The latest runtimes can be found [here](https://gitlab.com/bluehood/df_bench/pipelines?scope=branches&page=1) and can also be downloaded as text files.

### Building the benchmarks
Make sure there is an active `ROOT` in your environment (I have only tested compilation against a C++17 build of ROOT).<br>
Then from an empty directory just invoke `cmake` to build the benchmark executables: 
```bash
cmake /path/to/df_bench
cmake --build . [-- -j4]
```
[Google benchmark](https://github.com/google/benchmark) is downloaded and built with `df_bench` if it's not already installed in the system. 